---
# Playbook for DR using LSyncd

# ======================================================
#
# PRE-TASKS
#
# ======================================================

- name: 'Ensure dirs exist'
  hosts: all

  tasks:
    - name: 'Ensure dir paths exist'
      file:
        path: "{{ item }}"
        state: directory
      with_items:
        - '~/.scm/'
        - '/var/sync/archive/scm'
        - '/var/sync/scm'
      ignore_errors: false
      tags: 
        - normal
        - failover
        - failback

# ======================================================
#
# NORMAL (Alex as Primary)
#
# ======================================================

#
# Stop Services (stop everything to ensure we have no read/write activity)
#
-
  name: 'Normal: Stop Services'
  hosts: all
  remote_user: root

  tasks:
    - name: 'stop lsyncd service'
      service: name=lsyncd state=stopped
      ignore_errors: true

    - name: 'stop scm-manager services'
      shell: /var/scm/bin/scm-server stop
      ignore_errors: true
      
  tags: 
    - normal

#
# Archive local copy
#

- name: 'Archive working dir to archive'
  hosts: primary
  remote_user: root
  
  tasks:
    - name: 'backup current data via rsync...'
      action: shell rsync -a --delete /root/.scm/ /var/sync/archive/scm/
      ignore_errors: true
      tags: normal

#    - name: 'sync data to current via rsync...'
#      action: shell rsync -a --delete /var/sync/scm /var/scm
#      ignore_errors: true
#      tags: normal

-
  name: 'Normal: Start Services'
  hosts: primary
  remote_user: root

  tasks:
    - name: 'stop firewall service'
      service: name=firewalld state=stopped
      ignore_errors: true

    - name: 'start lsyncd service'
      service: name=lsyncd state=started
      ignore_errors: true

    - name: 'start scm-manager services'
      shell: /var/scm/bin/scm-server start
      ignore_errors: true

  tags: 
    - normal

#
# Verify LSYNC operations
#

# Compare
# -- Count total number of files
# -- Determine most recent file
# -- Verify most recent exists
# -- Checksum most recent files
# -- Compare recent file checksums
# If compare == fail
# -- notify administrator
# -- exit
# If compare == success
# -- Verify sync periodically
# If sync == fail,
# -- notify administrator


# ======================================================
#
# FAIL-OVER (Boyers as Primary)
#
# ======================================================

#
# Stop Services (stop everything to ensure we have no read/write activity)
#
-
  name: 'Failover: Stop Services'
  hosts: all
  remote_user: root

  tasks:
    - name: 'stop lsyncd service'
      service: name=lsyncd state=stopped
      ignore_errors: true

    - name: 'stop scm-manager services'
      shell: /var/scm/bin/scm-server stop
      ignore_errors: true

  tags:
    - failover

#
# Archive local copy
#

- name: 'Archive working dir to archive'
  hosts: secondary
  remote_user: root
  
  tasks:
    - name: 'backup current data via rsync...'
      action: shell rsync -a --delete /root/.scm/ /var/sync/archive/scm/
      ignore_errors: true
      tags: failover

    - name: 'sync data to current via rsync...'
      action: shell rsync -a --delete /var/sync/scm/ /root/.scm/
      ignore_errors: true
      tags: failover

#
# Start Services on "Boyers" (data is recent and safeguarded)
#

-
  name: 'Failover: Start Services'
  hosts: secondary
  remote_user: root

  tasks:
    - name: 'stop firewall service'
      service: name=firewalld state=stopped
      ignore_errors: true
    
    - name: 'start lsyncd service'
      service: name=lsyncd state=started
      ignore_errors: true

    - name: 'start scm-manager services'
      shell: /var/scm/bin/scm-server start
      ignore_errors: true

  tags:
    - failover


# ======================================================
#
# FAILBACK (Alex as Primary)
#
# ======================================================

#
# Stop Services (stop everything to ensure we have no read/write activity)
#
-
  name: 'Failback: Stop Services'
  hosts: all
  remote_user: root

  tasks:
    - name: 'stop lsyncd service'
      service: name=lsyncd state=stopped
      ignore_errors: true

    - name: 'stop scm-manager services'
      shell: /var/scm/bin/scm-server stop
      ignore_errors: true

  tags:
    - failback

#
# Verify Most Recent (do we have current data?)
#

#
# Archive local copy
#

- name: 'Archive working dir to archive'
  hosts: primary
  remote_user: root
  
  tasks:
    - name: 'backup current data via rsync...'
      action: shell rsync -a --delete /root/.scm/ /var/sync/archive/scm/
      ignore_errors: true
      tags: failback

    - name: 'sync data to current via rsync...'
      action: shell rsync -a --delete /var/sync/scm/ /root/.scm/
      ignore_errors: true
      tags: failback 

#
# Verify Archive (before we allow live syncing, do we have a good archive?)
#

#
# Start Services on "A" (data is recent and safeguarded)
#

-
  name: 'Failback: Start Services'
  hosts: primary
  remote_user: root

  tasks:
    - name: 'stop firewall service'
      service: name=firewalld state=stopped
      ignore_errors: true
      
    - name: 'start lsyncd service'
      service: name=lsyncd state=started
      ignore_errors: true

    - name: 'start scm-manager services'
      shell: /var/scm/bin/scm-server start
      ignore_errors: true

  tags:
    - failback
